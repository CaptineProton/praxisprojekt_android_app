package de.htw_berlin.bui.waterlevel.ws.MeasurementUtilizier;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import de.htw_berlin.bui.waterlevel.R;
import de.htw_berlin.bui.waterlevel.ws.LocationManager.LocationActivity;
import de.htw_berlin.bui.waterlevel.ws.Utils.MeasurementItems;

/**
 * Created by Sarah on 25.08.2015.
 */
public class Measurement extends Activity{

    /**
     * Members
     */

    private Button start, stop, capture, procced, back;
    private TextView nw_object, nw_water;
    private SurfaceView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.measurement_view);
        Context context = getApplicationContext();

        initSensors();
        initViewElements();
    }

    /**
     *  Initialisierung von View-Elementen und Events
     */
    private void initViewElements(){

        // TextViews
        nw_object = (TextView) findViewById(R.id.textView_nw_object_value);
        nw_water = (TextView) findViewById(R.id.textView_nw_water_value);

        // Buttons
        start = (Button) findViewById(R.id.button_start_cam);
        stop = (Button) findViewById(R.id.button_stop_cam);
        capture = (Button) findViewById(R.id.button_capture_cam);
        procced = (Button) findViewById(R.id.button_procced_2);
        back = (Button) findViewById(R.id.button_back);

        sv = (SurfaceView)findViewById(R.id.surfaceView);

        // Set button events
        final CameraAction cameraAction = new CameraAction(sv);


        start.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View arg0) {
                cameraAction.startCamera();
            }
        });
        stop.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View arg0) {
                cameraAction.startCamera();
            }
        });


        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                float focusDistances[] = new float[3];
                param.getFocusDistances(focusDistances);

                Orientation orientation = new Orientation(mValuesMagnet, mValuesAccel, mValuesOrientation, mRotationMatrix);
                d_angle = orientation.initOrientationMatrix();
                angle_y.setText(Double.valueOf(d_angle[0]).toString());
                angle_x.setText(Double.valueOf(d_angle[1]).toString());
                angle_z.setText(Double.valueOf(d_angle[2]).toString());
                 */

                float focusDistance[] = new float[3];
                Camera.Parameters param = cameraAction.getParam();
                param.getFocusDistances(focusDistance);

                float[] valuesOrientation = MeasurementItems.getValuesOrientation();
                float[] valuesMagnet = MeasurementItems.getValuesMagnet();
                float[] valuesAccel = MeasurementItems.getValuesAccel();
                float[] rotationMatix = MeasurementItems.getRotationMatrix();

                SensorAction sensorAction = new SensorAction(valuesMagnet, valuesAccel, valuesOrientation, rotationMatix);
                double[] angle = sensorAction.initOrientationMatrix();
            }
        });

        final Intent intentProceed = new Intent(this, Result.class);
        procced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentProceed);
            }
        });

        final Intent intentBack = new Intent(this, LocationActivity.class);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentBack);
            }
        });
    }

    /**
     * Initialisierung der Sensoren
     */
    private void initSensors(){
        SensorManager sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);

        final float[] valuesAccel = new float[3];
        final float[] valuesMagnet = new float[3];

        final SensorEventListener mEventListener = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {
                // Handle the events for which we registered
                switch (event.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        System.arraycopy(event.values, 0, valuesAccel, 0, 3);
                        break;

                    case Sensor.TYPE_MAGNETIC_FIELD:
                        System.arraycopy(event.values, 0, valuesMagnet, 0, 3);
                        break;
                }
            }
        };

        SensorAction.setListners(sensorManager, mEventListener);
    }


}
