package de.htw_berlin.bui.waterlevel.ar;

import java.io.File;

public class Constants {

    private static String WIKITUDE_LICENSE_KEY = "y9DfLYQXe3obkRHsyh6rO+9ZMLEC4FF4LmDp82XgIBxHsRibrZRd8eD1NubRVYcgj7p7DcVgNZnQYQcjCK9rsLQGvvyt34ABoj9TsduGPF/nUENAxUNgM/pPOb1qu1lrdXAogcolx75kCWJENhZ4uBa3HmLkwP3e8WPljy2KUKxTYWx0ZWRfX8982quGBTCUJExUSn0X6O/Wfknce4jmX6/STUYD3p6Ij6ukd1e+ygWGz/oeNt7hPUJiDANGHU93X78hlJPJf2uqeGa2H04RCgb54XKBzTOWqVN38wgstg6M/9B3cT9OLj9F4yLvsJoxE74kTEmu4BWK+RiY3X/OvPsUli7JgQc7cldXfa5A/mgqBPDq8+BsvuXLUljf4d3s33ndyv6e5DLD4VBquFYaP8PuTrxaw9/8MCzfQeObOVoJXQqYkKGPNEEeRYtbevsOt5BXp8PpnJ0BwFzHvWtQooY0/OT6gYNV2rILiVeEudwPS28F9jVbxxM6FPyhY09nzwgYLl0UomzBmKBorlfJax0XvJ4s5tHWmC2oOcSagrzUmEUn08JXlqlL/f25a6eASBh4q38dqaqQjZEaa9F6nPRtVCtKyfIM1c/UJm6j0DgTaybl4cLBIb38zOOTv6JgZdDFz/P6JO1zxVOIQOWyM5vwpw00TMapujV6oVvcZo0=";
    private static String ARCHITECT_WORLD_PATH = "assets" + File.separator + "index.html";

    public static String getWikitudeLicenseKey() {
        return WIKITUDE_LICENSE_KEY;
    }

    public static String getArchitectWorldPath() {
        return ARCHITECT_WORLD_PATH;
    }
}
