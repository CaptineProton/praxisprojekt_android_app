package de.htw_berlin.bui.waterlevel.ws.MeasurementUtilizier;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import de.htw_berlin.bui.waterlevel.R;
import de.htw_berlin.bui.waterlevel.ws.Utils.MeasurementItems;

/**
 * Created by Sarah on 26.08.2015.
 */
public class ResultActivity extends Activity {

    private TextView textView_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_view);
        Context context = getApplicationContext();

        textView_result = (TextView) findViewById(R.id.textView_result);

        calc();
    }

    private void calc() {

        // get all variables
        double length = MeasurementItems.getDistance(); // b
        double w_angle_in_degree= MeasurementItems.getAngle_water(); // tilt angle 1
        double o_angle_in_degree = MeasurementItems.getAngle_object(); // tilt angle 2
        double depth = MeasurementItems.getGroundDepth(); //



        double o_angle_in_gon = o_angle_in_degree * 200.0/180.0;
        double o_gon_in_radians = o_angle_in_gon * Math.PI/200.0;
        double o_tan = Math.tan(o_gon_in_radians);
        double o_tan_radians_in_gon = o_tan * 200.0/Math.PI;
        double o_gon_length = length * 200.0/180.0;
        double o_gon_result = (length / o_tan_radians_in_gon)*10;



        double w_angle_in_gon = w_angle_in_degree * 200.0/180.0;
        double w_gon_in_radians = w_angle_in_gon * Math.PI/200.0;
        double w_tan = Math.tan(w_gon_in_radians);
        double w_tan_radians_in_gon = w_tan * 200.0/Math.PI;
        double w_gon_length = length * 200.0/180.0;
        double w_gon_result = (length / w_tan_radians_in_gon)*10;

        double new_w = Math.round(depth-w_gon_result);


        settext(new_w, textView_result);

    }


    private void settext(double value, TextView textView){
        textView.setText(Double.toString(value));
    }
}
