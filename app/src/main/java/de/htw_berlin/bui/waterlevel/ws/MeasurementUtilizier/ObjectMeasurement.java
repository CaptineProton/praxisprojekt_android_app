package de.htw_berlin.bui.waterlevel.ws.MeasurementUtilizier;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.htw_berlin.bui.waterlevel.R;
import de.htw_berlin.bui.waterlevel.ws.LocationManager.LocationActivity;
import de.htw_berlin.bui.waterlevel.ws.Utils.MeasurementItems;

/**
 * Created by Sarah on 25.08.2015.
 */
public class ObjectMeasurement extends Activity{

    /**
     * Members
     */

    float[] mValuesMagnet       = new float[3];
    float[] mValuesAccel        = new float[3];
    float[] mValuesOrientation  = new float[3];
    float[] mRotationMatrix     = new float[9];

    private Button start, stop, capture, procced, back;
    private TextView nw_object;
    private SurfaceView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.object_measurement_view);
        Context context = getApplicationContext();

        initSensors();
        initViewElements();
    }

    /**
     *  Initialisierung von View-Elementen und Events
     */
    private void initViewElements(){

        // TextViews
        nw_object = (TextView) findViewById(R.id.textView_nw_object_value);

        // Buttons
        start = (Button) findViewById(R.id.button_start_cam);
        stop = (Button) findViewById(R.id.button_stop_cam);
        capture = (Button) findViewById(R.id.button_capture_cam);
        procced = (Button) findViewById(R.id.button_procced_2);
        back = (Button) findViewById(R.id.button_back);

        sv = (SurfaceView)findViewById(R.id.surfaceView);

        // Set button events
        final CameraAction cameraAction = new CameraAction(sv);


        start.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View arg0) {
                cameraAction.startCamera();
            }
        });
        stop.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View arg0) {
                cameraAction.stop_camera();
            }
        });


        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float focusDistance[] = new float[3];
                Camera.Parameters param = cameraAction.getParam();
                param.getFocusDistances(focusDistance);

                float[] valuesOrientation = MeasurementItems.getValuesOrientation();
                float[] valuesMagnet = MeasurementItems.getValuesMagnet();
                float[] valuesAccel = MeasurementItems.getValuesAccel();
                float[] rotationMatix = MeasurementItems.getRotationMatrix();

                SensorAction sensorAction = new SensorAction(valuesMagnet, valuesAccel, valuesOrientation, rotationMatix);
                double[] angle = initOrientationMatrix();

                settext(angle, nw_object);
            }
        });

        final Intent intentProceed = new Intent(this, WaterMeasurement.class);
        procced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraAction.stop_camera();
                startActivity(intentProceed);
            }
        });

        final Intent intentBack = new Intent(this, LocationActivity.class);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraAction.stop_camera();
                startActivity(intentBack);
            }
        });
    }

    private void settext(double[] angle, TextView textView){
        // TODO: An dieser Stelle den richtigen Winkel aus "angle" auswählen und in String konvertieren
        double pitch = 90 + angle[1];
        MeasurementItems.setAngle_object(pitch);
        textView.setText(Double.toString(pitch));
    }

    /**
     * Initialisierung der Sensoren
     */
    private void initSensors(){
        final SensorManager sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);

        final SensorEventListener mEventListener = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {
                // Handle the events for which we registered
                switch (event.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        System.arraycopy(event.values, 0, mValuesAccel, 0, 3);
                        break;

                    case Sensor.TYPE_MAGNETIC_FIELD:
                        System.arraycopy(event.values, 0, mValuesMagnet, 0, 3);
                        break;
                }

            }
        };

        SensorAction.setListners(sensorManager, mEventListener);
    }

    public double[] initOrientationMatrix(){

        boolean success = SensorManager.getRotationMatrix(mRotationMatrix, null, mValuesAccel, mValuesMagnet);

        SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);

        // azimuth =  rotation around the Z axis.
        // pitch =  rotation around the X axis.
        // roll = rotation around the Y axis
        double azimuth = Math.round(Math.toDegrees(mValuesOrientation[0]));
        double pitch = Math.round(Math.toDegrees(mValuesOrientation[1]));
        double roll = Math.round(Math.toDegrees(mValuesOrientation[2]));
        double values[] = new double[3];
        values[0] = roll;
        values[1] = pitch;
        values[2] = azimuth;

        return values;

    }

    // Register the event listener and sensor type.
    public static void setListners(SensorManager sensorManager, SensorEventListener mEventListener)
    {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

}
