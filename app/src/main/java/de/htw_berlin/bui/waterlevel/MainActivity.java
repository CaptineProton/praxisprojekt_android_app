package de.htw_berlin.bui.waterlevel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import de.htw_berlin.bui.waterlevel.ar.ArActivity;
import de.htw_berlin.bui.waterlevel.ws.LocationManager.LocationActivity;


/*
    Haupteinstiegspunkt der Anwendung für AR- und Wasserstand-App
 */
public class MainActivity extends Activity {

    private Button arApp;
    private Button wsApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);
        // init elements
        initViewElements();

        // define intents to go to application page
        final Intent intent_AR = new Intent(this, ArActivity.class);
        final Intent intent_WS = new Intent(this, LocationActivity.class);

        // manage click-events
        arApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start AR App @ on button click
                startActivity(intent_AR);
            }
        });

        wsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent_WS);
            }
        });

        testMath();
    }

    /*
        Initialisiert View-Elemente
     */
    private void initViewElements(){
        arApp = (Button) findViewById(R.id.button_AR);
        wsApp = (Button) findViewById(R.id.button_waterline);
    }

    private void testMath() {
        // gons for object
        double length = 4.84;
        double o_angle_in_degree = 22.00;
        double depth = 28.37;

        double o_angle_in_gon = o_angle_in_degree * 200.0 / 180.0;
        double o_gon_in_radians = o_angle_in_gon * Math.PI / 200.0;
        double o_tan = Math.tan(o_gon_in_radians);
        double o_tan_radians_in_gon = o_tan * 200.0 / Math.PI;
        double o_gon_length = length * 200.0 / 180.0;
        double o_gon_result = (length / o_tan_radians_in_gon)*10;


        double w_angle_in_degree = 10.00;

        double w_angle_in_gon = w_angle_in_degree * 200.0 / 180.0;
        double w_gon_in_radians = w_angle_in_gon * Math.PI / 200.0;
        double w_tan = Math.tan(w_gon_in_radians);
        double w_tan_radians_in_gon = w_tan * 200.0 / Math.PI;
        double w_gon_length = length * 200.0 / 180.0;
        double w_gon_result = (length / w_tan_radians_in_gon)*10;

        double new_w = depth - w_gon_result;
    }
}
