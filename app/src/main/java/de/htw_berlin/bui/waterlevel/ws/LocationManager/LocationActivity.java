package de.htw_berlin.bui.waterlevel.ws.LocationManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;

import de.htw_berlin.bui.waterlevel.R;
import de.htw_berlin.bui.waterlevel.ws.MeasurementUtilizier.ObjectMeasurement;
import de.htw_berlin.bui.waterlevel.ws.Utils.MeasurementItems;

/**
 * Created by Sarah on 24.08.2015.
 */
public class LocationActivity extends Activity{

    Context mContext;
    TextView result_Text;
    Button proceed_btn;

    /** Instantiate the interface and set the context */
    LocationActivity(Context c) {
        mContext = c;
    }

    public LocationActivity(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_view);
        Context context = getApplicationContext();

        startWebView(context);
        initViewElement();
    }

    private WebChromeClient MyWebChromeClient = new WebChromeClient() {
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            try {
                // "message" is the text shown in alert box
                Log.v("message", java.net.URLDecoder.decode(message, "UTF-8"));
                double distance = Double.parseDouble(message);
                MeasurementItems.setDistance(distance);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return true;
        }
    };


    /**
     * Die im Skript berechnete Distanz zwischen Nutzer und Messobjekt wird aus dem Skript
     * ausgelesen und den aktuellen Messgrößen hinzugefügt.
     * @param str_Distance
     */
    @JavascriptInterface   // must be added for API 17 or higher
    public void onData(String str_Distance) {
        float distance = Float.parseFloat(str_Distance);
        MeasurementItems.setDistance(distance);
        result_Text.setText(str_Distance);
    }

    /**
     * Initialisierung der WebView
     * @param context
     */
    private void startWebView(Context context){
        WebView mWebView = (WebView) findViewById(R.id.webView);
        // Brower niceties -- pinch / zoom, follow links in place
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        // Below required for geolocation

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setGeolocationEnabled(true);
        mWebView.clearCache(true);
        // Load google.com
        String url = "http://magun.htw-berlin.de/~ffischer/distance/";
        mWebView.loadUrl(url);
        mWebView.reload();
        mWebView.setWebChromeClient(MyWebChromeClient);
        //mWebView.addJavascriptInterface(this, "android");
        //mWebView.loadUrl("javascript:android.onData(functionThatReturnsSomething)");

    }

    /**
     * Initialisert View Elemente und Events
     */
    private void initViewElement(){
        result_Text = (TextView) findViewById(R.id.textView_distanceValue);
        proceed_btn = (Button) findViewById(R.id.button_proceed);

        final Intent intent = new Intent(this, ObjectMeasurement.class);
        proceed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start a new intent
                startActivity(intent);
            }
        });
    }
}
