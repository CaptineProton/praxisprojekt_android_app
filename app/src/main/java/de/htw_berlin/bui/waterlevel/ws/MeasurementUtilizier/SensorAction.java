package de.htw_berlin.bui.waterlevel.ws.MeasurementUtilizier;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Sarah on 25.08.2015.
 */
public class SensorAction  {

    float[] mValuesMagnet       = new float[3];
    float[] mValuesAccel        = new float[3];
    float[] mValuesOrientation  = new float[3];
    float[] mRotationMatrix     = new float[9];

    public SensorAction(float[] mValuesMagnet, float[] mValuesAccel,
                       float[] mValuesOrientation, float[] mRotationMatrix){
        this.mValuesMagnet      = mValuesMagnet;
        this.mValuesAccel       = mValuesAccel;
        this.mValuesOrientation = mValuesOrientation;
        this.mRotationMatrix    = mRotationMatrix;
    }

    public double[] initOrientationMatrix(){

        boolean success = SensorManager.getRotationMatrix(mRotationMatrix, null, mValuesAccel, mValuesMagnet);

        SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);

        // azimuth =  rotation around the Z axis.
        // pitch =  rotation around the X axis.
        // roll = rotation around the Y axis
        double azimuth = Math.round(Math.toDegrees(mValuesOrientation[0]));
        double pitch = Math.round(Math.toDegrees(mValuesOrientation[1]));
        double roll = Math.round(Math.toDegrees(mValuesOrientation[2]));
        double values[] = new double[3];
        values[0] = roll;
        values[1] = pitch;
        values[2] = azimuth;

        return values;

    }

    // Register the event listener and sensor type.
    public static void setListners(SensorManager sensorManager, SensorEventListener mEventListener)
    {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

 }
