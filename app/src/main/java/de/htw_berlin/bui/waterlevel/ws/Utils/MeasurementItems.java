package de.htw_berlin.bui.waterlevel.ws.Utils;

/**
 * Created by Sarah on 25.08.2015.
 */
public class MeasurementItems {

    // members
    private static double distance = 04.85; //default value in meter
    private static float[] mValuesMagnet      = new float[3];
    private static float[] mValuesAccel       = new float[3];
    private static float[] mValuesOrientation = new float[3];
    private static float[] mRotationMatrix    = new float[9];

    private static double angle_object;
    private static double angle_water;

    private static double groundDepth =  -10.00; // default value in

    /**
     * Aktuelle Distance zwischen Nutzer und Messobjekt (in Metern) wird
     * zurückgegeben
     * @return distance
     */
    public static double getDistance(){
         return  distance;
    }

    /**
     * Setzt die aktuelle Distance zwischen Nutzer und Messobjekt (in Metern)
     * @param _distance
     */
    public static void setDistance(double _distance){
        distance = _distance;
    }

    /**
     * Aktuellen Werte des Magnetsensors werden zurückgeliefert
     * @return
     */
    public static float[] getValuesMagnet(){
        return mValuesMagnet;
    }

    /**
     * Setzt die aktuellen Werte des Magnetsensors
     * @param valuesMagnet
     */
    public static void setValuesMagnet(float[] valuesMagnet){
        mValuesMagnet = valuesMagnet;
    }

    public static float[] getValuesAccel(){
        return mValuesAccel;
    }

    public static void setValuesAccel(float[] valuesAccel){
        mValuesAccel = valuesAccel;
    }

    public static  float[] getValuesOrientation() {
        return mValuesOrientation;
    }

    public static void setValuesOrientation(float[] valuesOrientation){
        mValuesOrientation = valuesOrientation;
    }

    public static float[] getRotationMatrix(){
        return mRotationMatrix;
    }

    public static void setRotationMatrix(float[] rotationMatrix){
        mRotationMatrix = rotationMatrix;
    }

    public static double getAngle_object(){
        return angle_object;
    }

    public static void setAngle_object(double angle){
        angle_object = angle;
    }


    public static double getAngle_water(){
        return angle_water;
    }

    public static void setAngle_water(double angle){
        angle_water = angle;
    }

    public static double getGroundDepth(){
        return groundDepth;
    }

    public static void setGroundDepth(double depth){
        groundDepth = depth;
    }
}
